import { IScore } from "./IScore";
import { Player } from "../Player";
import { ClassicScore } from "./ClassicScore";
import { AdvantageScore } from "./AdvantageScore";

export class EqualityScore implements IScore {
  player1: Player;
  player2: Player;

  constructor(player1: Player, player2: Player) {
    this.player1 = player1;
    this.player2 = player2;
  }

  playerWin(winningPlayer: Player): IScore {
    winningPlayer.incrementScore();

    if (this.player1.score > 3 || this.player2.score > 3) {
      return new AdvantageScore(this.player1, this.player2);
    }

    return new ClassicScore(this.player1, this.player2);
  }

  getCurrentScore(): string {
    switch (this.player1.score) {
      case 0:
        return "Love-All";
      case 1:
        return "Fifteen-All";
      case 2:
        return "Thirty-All";
      default:
        return "Deuce";
    }
  }
}
