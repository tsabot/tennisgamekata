import { IScore } from "./IScore";
import { Player } from "../Player";
import { WinScore } from "./WinScore";
import { EqualityScore } from "./EqualityScore";

export class ClassicScore implements IScore {
  player1: Player;
  player2: Player;

  constructor(player1: Player, player2: Player) {
    this.player1 = player1;
    this.player2 = player2;
  }

  playerWin(winningPlayer: Player): IScore {
    winningPlayer.incrementScore();

    if (this.player1.score === 4 || this.player2.score === 4) {
      return new WinScore(this.player1, this.player2);
    }

    if (this.player1.score === this.player2.score) {
      return new EqualityScore(this.player1, this.player2);
    }

    return this;
  }
  getCurrentScore(): string {
    const player1ScoreToDisplay = this.displayClassicScore(this.player1.score);
    const player2ScoreToDisplay = this.displayClassicScore(this.player2.score);

    return `${player1ScoreToDisplay}-${player2ScoreToDisplay}`;
  }

  private displayClassicScore(playerScore: number) {
    switch (playerScore) {
      case 0:
        return "Love";
      case 1:
        return "Fifteen";
      case 2:
        return "Thirty";
      default:
        return "Forty";
    }
  }
}
