import { IScore } from "./IScore";
import { Player } from "../Player";
import { WinScore } from "./WinScore";
import { EqualityScore } from "./EqualityScore";

export class AdvantageScore implements IScore {
  player1: Player;
  player2: Player;

  constructor(player1: Player, player2: Player) {
    this.player1 = player1;
    this.player2 = player2;
  }

  playerWin(winningPlayer: Player): IScore {
    winningPlayer.incrementScore();

    //TODO: à mettre en param ?
    const differencesInScore: number = this.player1.score - this.player2.score;

    if (differencesInScore >= 2 || differencesInScore <= -2) {
      return new WinScore(this.player1, this.player2);
    }

    return new EqualityScore(this.player1, this.player2);
  }
  getCurrentScore(): string {
    const differencesInScore: number = this.player1.score - this.player2.score;

    switch (differencesInScore) {
      case 1:
        return `Advantage ${this.player1.name}`;
      default:
        return `Advantage ${this.player2.name}`;
    }
  }
}
