import { Player } from "../Player";

export interface IScore {
  player1: Player;
  player2: Player;

  playerWin(playerName: Player): IScore;
  getCurrentScore(): string;
}
