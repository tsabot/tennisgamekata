import { IScore } from "./IScore";
import { Player } from "../Player";

export class WinScore implements IScore {
  player1: Player;
  player2: Player;

  constructor(player1: Player, player2: Player) {
    this.player1 = player1;
    this.player2 = player2;
  }

  playerWin(_playerName: Player) {
    return this;
  }

  getCurrentScore(): string {
    const differencesInScore: number = this.player1.score - this.player2.score;

    if (differencesInScore >= 2) {
      return `Win for ${this.player1.name}`;
    }

    return `Win for ${this.player2.name}`;
  }
}
