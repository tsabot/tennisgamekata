export class Player {
  private _name: string;
  private _score: number = 0;

  public get name(): string {
    return this._name;
  }
  public set name(value: string) {
    this._name = value;
  }

  public get score(): number {
    return this._score;
  }
  public set score(value: number) {
    this._score = value;
  }

  constructor(playerName: string) {
    this._name = playerName;
  }

  incrementScore(): void {
    this.score += 1;
  }
}
