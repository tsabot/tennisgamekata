import { TennisGame } from "./TennisGame";
import { Player } from "./Player";
import { IScore } from "./ScoreStates/IScore";
import { EqualityScore } from "./ScoreStates/EqualityScore";

export class TennisGame1 implements TennisGame {
  private player1: Player;
  private player2: Player;
  private scoreState: IScore;

  constructor(player1Name: string, player2Name: string) {
    this.player1 = new Player(player1Name);
    this.player2 = new Player(player2Name);

    this.scoreState = new EqualityScore(this.player1, this.player2);
  }

  wonPoint(playerName: string): void {
    const winningPlayer =
      this.player1.name === playerName ? this.player1 : this.player2;

    this.scoreState = this.scoreState.playerWin(winningPlayer);
  }

  getScore(): string {
    return this.scoreState.getCurrentScore();
  }
}
